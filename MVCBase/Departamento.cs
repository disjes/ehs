//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MVCBase
{
    using System;
    using System.Collections.Generic;
    
    public partial class Departamento
    {
        public Departamento()
        {
            this.Investigacion = new HashSet<Investigacion>();
            this.Representante = new HashSet<Representante>();
        }
    
        public int idDepartemento { get; set; }
        public string nombre { get; set; }
    
        public virtual ICollection<Investigacion> Investigacion { get; set; }
        public virtual ICollection<Representante> Representante { get; set; }
    }
}
