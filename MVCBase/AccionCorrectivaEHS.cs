//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MVCBase
{
    using System;
    using System.Collections.Generic;
    
    public partial class AccionCorrectivaEHS
    {
        public int idAccionCorrectiva { get; set; }
        public Nullable<int> incidenteDano { get; set; }
        public string accionDescripcion { get; set; }
        public Nullable<int> responsableEHS { get; set; }
    
        public virtual IncidentesDanos IncidentesDanos { get; set; }
        public virtual Representante Representante { get; set; }
    }
}
