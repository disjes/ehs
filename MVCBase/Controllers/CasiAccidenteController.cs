﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCBase;
using System.IO;

namespace MVCBase.Controllers
{
    public class CasiAccidenteController : Controller
    {
        private EHSEntities db = new EHSEntities();

        // GET: /CasiAccidente/
        public ActionResult Index()
        {
            ViewBag.seccion = "Incidentes";
            ViewBag.view = "Listado de Casi Incidentes";
            var casiaccidente = db.CasiAccidente.Include(c => c.Lugar1).Include(c => c.Representante);
            return View(casiaccidente.ToList());
        }

        public ActionResult Adjunto(FormCollection formData)
        {
            HttpPostedFileBase file = Request.Files[0];
            string mPath = "";
            if (file.FileName != "")
            {
                mPath = Server.MapPath("~/") + "Views\\CasiAccidente\\Files\\" + file.FileName;
                file.SaveAs(mPath);
            }
            var adjunto = new AdjuntoCasiIncidente();
            adjunto.fecha = DateTime.Now;
            adjunto.idCasiIncidente = Convert.ToInt32(Request["hiddenId"]);
            adjunto.usuario = User.Identity.Name;
            adjunto.comentario = Request["comment"];
            if (file.FileName != "") adjunto.path = "Files\\" + file.FileName; else adjunto.path = "";
            db.AdjuntoCasiIncidente.Add(adjunto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Cerrar(int id)
        {
            CasiAccidente mCasiAccidente = db.CasiAccidente.First(x => x.idCasiIncidente == id);
            mCasiAccidente.fechaCierre = DateTime.Now;
            mCasiAccidente.usuarioCerro = User.Identity.Name;
            db.Entry(mCasiAccidente).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public FileResult GetFile(int id)
        {
            string path = Server.MapPath("~/") + "Views\\CasiAccidente\\" + db.AdjuntoCasiIncidente.First(x => x.idAdjunto == id).path;
            return File(path, System.Net.Mime.MediaTypeNames.Application.Octet, Path.GetFileName(path));
        }

        public ActionResult PartialAdjuntos(int id)
        {
            var casiAccidente = db.CasiAccidente.First(x => x.idCasiIncidente == id);
            ViewBag.titulo = casiAccidente.titulo;
            ViewBag.fecha = casiAccidente.fechaHora;
            ViewBag.lugar = casiAccidente.Lugar1.descripcion;
            ViewBag.personal = casiAccidente.personalApoyo;
            ViewBag.representante = casiAccidente.Representante.nombre;
            var adjunto = db.AdjuntoCasiIncidente.Where(x => x.idCasiIncidente == id);
            return PartialView("_PartialAdjuntos", casiAccidente);
        }

        // GET: /CasiAccidente/Details/5
        public ActionResult Details(int? id)
        {
            rptCasiAccidente mRep = new rptCasiAccidente();
            mRep.SetParameterValue("idCasiAccidente", id);
            mRep.SetDatabaseLogon("ehsusr", "ehs123", "192.168.95.21", "EHS", true);
            //mRep.SetDatabaseLogon("sarahtest", "opc123", "192.168.95.11", "EHS", true);
            return new FileStreamResult(mRep.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat), "application/pdf");
        }

        // GET: /CasiAccidente/Create
        public ActionResult Create()
        {
            ViewBag.seccion = "Incidentes";
            ViewBag.view = "Crear Casi Incidente";
            ViewBag.lugar = new SelectList(db.Lugar, "idLugar", "nombre");
            ViewBag.reponsobleEHS = new SelectList(db.Representante.Where(x => x.tipoRepresentante == "1"), "idRepresentante", "nombre");
            return View();
        }

        // POST: /CasiAccidente/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="idCasiIncidente,titulo,lugar,actoInseguro,equipoInseguro,condicionInsegura,usoInseguroEquipo,otro,descripcionPeligro,descripcionCondicion,analisis,medidasCorrectivas,personalApoyo,reponsobleEHS")] CasiAccidente casiaccidente)
        {
            if (ModelState.IsValid)
            {
                casiaccidente.fechaHora = null;
                casiaccidente.usuario = User.Identity.Name;
                db.CasiAccidente.Add(casiaccidente);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.seccion = "Incidentes";
            ViewBag.view = "Crear Casi Incidente";
            ViewBag.lugar = new SelectList(db.Lugar, "idLugar", "nombre", casiaccidente.lugar);
            ViewBag.reponsobleEHS = new SelectList(db.Representante.Where(x => x.tipoRepresentante == "1"), "idRepresentante", "nombre", casiaccidente.reponsobleEHS);
            return View(casiaccidente);
        }

        // GET: /CasiAccidente/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CasiAccidente casiaccidente = db.CasiAccidente.Find(id);
            if (casiaccidente == null)
            {
                return HttpNotFound();
            }
            ViewBag.seccion = "Incidentes";
            ViewBag.view = "Editar Casi Incidente";
            ViewBag.lugar = new SelectList(db.Lugar, "idLugar", "nombre", casiaccidente.lugar);
            ViewBag.reponsobleEHS = new SelectList(db.Representante.Where(x => x.tipoRepresentante == "1"), "idRepresentante", "nombre", casiaccidente.reponsobleEHS);
            return View(casiaccidente);
        }

        // POST: /CasiAccidente/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="idCasiIncidente,titulo,fechaHora,lugar,actoInseguro,equipoInseguro,condicionInsegura,usoInseguroEquipo,otro,descripcionPeligro,descripcionCondicion,analisis,medidasCorrectivas,personalApoyo,reponsobleEHS,fechaCierre,usuario,usuarioCerro")] CasiAccidente casiaccidente)
        {
            if (ModelState.IsValid)
            {
                db.Entry(casiaccidente).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.seccion = "Incidentes";
            ViewBag.view = "Editar Casi Incidente";
            ViewBag.lugar = new SelectList(db.Lugar, "idLugar", "nombre", casiaccidente.lugar);
            ViewBag.reponsobleEHS = new SelectList(db.Representante.Where(x => x.tipoRepresentante == "1"), "idRepresentante", "nombre", casiaccidente.reponsobleEHS);
            return View(casiaccidente);
        }

        // GET: /CasiAccidente/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CasiAccidente casiaccidente = db.CasiAccidente.Find(id);
            if (casiaccidente == null)
            {
                return HttpNotFound();
            }
            return View(casiaccidente);
        }

        // POST: /CasiAccidente/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CasiAccidente casiaccidente = db.CasiAccidente.Find(id);
            db.CasiAccidente.Remove(casiaccidente);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
