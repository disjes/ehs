﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCBase.Controllers
{
    public class TestController : Controller
    {

        private SARAHEntities db = new SARAHEntities();
        private EHSEntities edb = new EHSEntities();
        //
        // GET: /Test/
        public ActionResult Index()
        {            
            ViewBag.combo = new SelectList(db.TEST(10, System.Data.Entity.Core.Objects.MergeOption.PreserveChanges), "idTitulo", "nroContenedor");
            return View();
        }

        //
        // GET: /Test/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Test/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Test/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Test/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Test/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Test/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Test/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
