﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCBase;

namespace MVCBase.Controllers
{
    public class NoConformidadController : Controller
    {
        private EHSEntities db = new EHSEntities();

        // GET: /NoConformidad/
        public ActionResult Index()
        {
            var noconformidad = db.NoConformidad.Include(n => n.Buque1).Include(n => n.Inspeccion);
            return View(noconformidad.ToList());
        }

        // GET: /NoConformidad/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NoConformidad noconformidad = db.NoConformidad.Find(id);
            if (noconformidad == null)
            {
                return HttpNotFound();
            }
            return View(noconformidad);
        }

        // GET: /NoConformidad/Create
        public ActionResult Create()
        {
            ViewBag.buque = new SelectList(db.Buque, "idBuque", "nombre");
            ViewBag.idInspeccion = new SelectList(db.Inspeccion, "idInspeccion", "visita");
            return View();
        }

        // POST: /NoConformidad/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="idNoConformidad,idInspeccion,fecha,nroContenedor,buque,noSello,danoContenedor,etiquetaMercancia,danoguias,otro,detalles")] NoConformidad noconformidad)
        {
            if (ModelState.IsValid)
            {
                db.NoConformidad.Add(noconformidad);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.buque = new SelectList(db.Buque, "idBuque", "nombre", noconformidad.buque);
            ViewBag.idInspeccion = new SelectList(db.Inspeccion, "idInspeccion", "visita", noconformidad.idInspeccion);
            return View(noconformidad);
        }

        // GET: /NoConformidad/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NoConformidad noconformidad = db.NoConformidad.Find(id);
            if (noconformidad == null)
            {
                return HttpNotFound();
            }
            ViewBag.buque = new SelectList(db.Buque, "idBuque", "nombre", noconformidad.buque);
            ViewBag.idInspeccion = new SelectList(db.Inspeccion, "idInspeccion", "visita", noconformidad.idInspeccion);
            return View(noconformidad);
        }

        // POST: /NoConformidad/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="idNoConformidad,idInspeccion,fecha,nroContenedor,buque,noSello,danoContenedor,etiquetaMercancia,danoguias,otro,detalles")] NoConformidad noconformidad)
        {
            if (ModelState.IsValid)
            {
                db.Entry(noconformidad).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.buque = new SelectList(db.Buque, "idBuque", "nombre", noconformidad.buque);
            ViewBag.idInspeccion = new SelectList(db.Inspeccion, "idInspeccion", "visita", noconformidad.idInspeccion);
            return View(noconformidad);
        }

        // GET: /NoConformidad/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NoConformidad noconformidad = db.NoConformidad.Find(id);
            if (noconformidad == null)
            {
                return HttpNotFound();
            }
            return View(noconformidad);
        }

        // POST: /NoConformidad/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            NoConformidad noconformidad = db.NoConformidad.Find(id);
            db.NoConformidad.Remove(noconformidad);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
