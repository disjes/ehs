﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCBase;
using System.IO;
using MVCBase.BuquesSrv;

namespace MVCBase.Controllers
{
    public class InspeccionController : Controller
    {
        private EHSEntities db = new EHSEntities();

        // GET: /Default2/
        [Authorize(Roles = "ehs_insp")]

        public ActionResult Index()
        {
            ViewBag.seccion = "Inspecciones";
            ViewBag.view = "Listado de Inspecciones";
            var inspeccion = db.Inspeccion.Include(i => i.Buque1).Include(i => i.Representante).Include(i => i.Representante1);
            return View(inspeccion.ToList());
        }

        // GET: /Default2/Details/5
        [Authorize(Roles = "ehs_insp")]
        public FileStreamResult Details(int? id)
        {
            rptInspeccion mRep = new rptInspeccion();
            mRep.SetParameterValue("idInspeccion", id);
            mRep.SetDatabaseLogon("ehsusr", "ehs123", "192.168.95.21", "EHS", true);
            //mRep.SetDatabaseLogon("sarahtest", "opc123", "192.168.95.11", "EHS", true);
            return new FileStreamResult(mRep.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat), "application/pdf");
        }

        // GET: /Default2/Details/5
        [Authorize(Roles = "ehs_insp")]
        public FileStreamResult NoConfReport(int id)
        {
            NoConformidadReport mRep = new NoConformidadReport();
            mRep.SetParameterValue("idNoConformidad", id);
            mRep.SetDatabaseLogon("ehsusr", "ehs123", "192.168.95.21", "EHS", true);
            return new FileStreamResult(mRep.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat), "application/pdf");
        }

        // GET: /Default2/Create
        [Authorize(Roles = "ehs_insp")]
        public ActionResult Create()
        {
            ViewBag.seccion = "Inspecciones";
            ViewBag.view = "Crear Inspeccion";
            //HERE BUQUES
            ViewBag.buque = new SelectList(new BuquesSrv.N4Client().GetVessels(), "vesselID", "vesselName"); 
            //ViewBag.buque = new SelectList(db.Buque, "idBuque", "nombre");           
            ViewBag.representanteTer = new SelectList(db.Representante.Where(x => x.tipoRepresentante == "1"), "idRepresentante", "nombre");
            ViewBag.representanteBuq = new SelectList(db.Representante.Where(x => x.tipoRepresentante == "2"), "idRepresentante", "nombre");            
            ViewBag.segmentos = new List<Segmentos>(db.Segmentos.Where(x => x.activa == "A")).OrderBy(x => x.orden).ToList();
            ViewBag.preguntas = new List<Preguntas>(db.Preguntas.Where(x => x.activa == "A").Where(x => x.Segmentos.activa == "A"));
            ViewBag.turno = new SelectList(new string[] {"A", "B", "C"});
            Inspeccion mInspeccion = new Inspeccion();
            mInspeccion.Respuestas = createRespuestas();
            return View(mInspeccion);
        }

        public List<Respuestas> createRespuestas()
        {
            List<Respuestas> mRespuestas = new List<Respuestas>();
            List<Preguntas> mPreguntas = db.Preguntas.ToList();
            foreach (Preguntas mPregunta in mPreguntas)
            {
                Respuestas mResp = new Respuestas();
                mResp.pregunta = mPregunta.idPregunta;
                mResp.tipoRespuesta = mPregunta.tipoPregunta;
                mRespuestas.Add(mResp);
            }
            return mRespuestas;
        }

        // POST: /Default2/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "ehs_insp")]
        public ActionResult Create([Bind(Include="idInspeccion,buque,visita,representanteTer,representanteBuq,Respuestas")] Inspeccion inspeccion)
        {
            var buques = new BuquesSrv.N4Client().GetVessels().ToList();
            if (ModelState.IsValid)
            {
                if (DateTime.Now >= DateTime.Today.AddHours(7) && DateTime.Now < DateTime.Today.AddHours(15))
                    inspeccion.turno = "A";
                if (DateTime.Now > DateTime.Today.AddHours(15) && DateTime.Now < DateTime.Today.AddHours(23))
                    inspeccion.turno = "B";
                if (DateTime.Now > DateTime.Today.AddHours(23) ||
                   (DateTime.Now >= DateTime.Today.AddHours(0) && DateTime.Now < DateTime.Today.AddHours(7)))
                    inspeccion.turno = "C";
                //HERE BUQUES
                createBuqueIfNotExists(inspeccion.buque, buques);
                inspeccion.fecha = null;
                inspeccion.visita = buques.First(x => x.vesselID == inspeccion.buque).vesselvisitId;
                inspeccion.usuario = User.Identity.Name;
                db.Inspeccion.Add(inspeccion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            //HERE BUQUES
            ViewBag.buque = new SelectList(buques, "vesselID", "vesselName");
            ViewBag.representanteTer = new SelectList(db.Representante.Where(x => x.tipoRepresentante == "1"), "idRepresentante", "nombre");
            ViewBag.representanteBuq = new SelectList(db.Representante.Where(x => x.tipoRepresentante == "2"), "idRepresentante", "nombre");
            ViewBag.segmentos = new List<Segmentos>(db.Segmentos.Where(x => x.activa == "A")).OrderBy(x => x.orden).ToList();
            ViewBag.preguntas = new List<Preguntas>(db.Preguntas.Where(x => x.activa == "A"));
            ViewBag.turno = new SelectList(new string[] { "A", "B", "C" });
            Inspeccion mInspeccion = new Inspeccion();
            mInspeccion.Respuestas = createRespuestas();
            return View(mInspeccion);
        }

        private void createBuqueIfNotExists(string id, List<vwVesselsVisit> buques)
        {
            if (db.Buque.FirstOrDefault(x => x.idBuque == id) == null)
            {
                var buque = new Buque();
                buque.idBuque = id;
                buque.nombre = buques.First(x => x.vesselID == id).vesselName;
                db.Buque.Add(buque);
                db.SaveChanges();
            }
        }

        // GET: /Default2/Edit/5
        [Authorize(Roles = "ehs_insp")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inspeccion inspeccion = db.Inspeccion.Find(id);
            if (inspeccion == null)
            {
                return HttpNotFound();
            }
            ViewBag.seccion = "Inspecciones";
            ViewBag.view = "Editar Inspeccion";
            //HERE BUQUES
            ViewBag.buque = inspeccion.Buque1.nombre;
            //ViewBag.buque = new SelectList(new BuquesSrv.Service1Client().GetVessels(), "vesselID", "vesselName");   
            //ViewBag.representanteTer = new SelectList(db.Representante, "idRepresentante", "nombre", inspeccion.representanteTer);
            //ViewBag.representanteBuq = new SelectList(db.Representante, "idRepresentante", "nombre", inspeccion.representanteBuq);
            ViewBag.segmentos = new List<Segmentos>(db.Segmentos.Where(x => x.activa == "A")).OrderBy(x => x.orden).ToList();
            //ViewBag.preguntas = new List<Preguntas>(db.Preguntas);
            ViewBag.turno = new SelectList(new string[] { "A", "B", "C" });
            List<int> codesId = inspeccion.Respuestas.Select(x => x.pregunta).ToList();
            //List<Preguntas> mPreg = db.Preguntas.Where(x => codesId.Contains(x.idPregunta)).ToList();
            ViewBag.preguntas = db.Preguntas.Where(x => codesId.Contains(x.idPregunta)).Where(x => x.activa == "A").ToList();
            //inspeccion.Respuestas = createRespuestas();
            //inspeccion.Respuestas = db.Respuestas.Where(x => x.inspecccion == id).ToList();
            return View(inspeccion);
        }

        // POST: /Default2/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "ehs_insp")]
        public ActionResult Edit([Bind(Include="idInspeccion,buque,fecha,visita,representanteTer,representanteBuq,turno,Respuestas")] Inspeccion mInspeccion)
        {
            //var buques = new BuquesSrv.Service1Client().GetVessels().ToList();
            Inspeccion inspeccion = db.Inspeccion.Find(mInspeccion.idInspeccion);
            if (ModelState.IsValid)
            {
                db.Entry(inspeccion).CurrentValues.SetValues(mInspeccion);               
                //db.Entry(mInspeccion).State = EntityState.Modified;
                foreach (var mRespuesta in mInspeccion.Respuestas)
                {
                    Respuestas respuesta = db.Respuestas.Find(mRespuesta.idRespuesta);
                    if (mRespuesta.vFecha == null) mRespuesta.vFecha = DateTime.Now;
                    if (mRespuesta.vTexto == null) mRespuesta.vTexto = "";
                    if (mRespuesta.vSeleccion == null) mRespuesta.vSeleccion = "";
                    //db.Entry(mRespuesta).State = EntityState.Modified;
                    db.Entry(respuesta).CurrentValues.SetValues(mRespuesta);
                }
                try
                {
                    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-GB");
                    //HERE BUQUES
                    //createBuqueIfNotExists(inspeccion.buque, buques);
                    //inspeccion.visita = buques.First(x => x.vesselID == inspeccion.buque).vesselvisitId;
                    int af = db.SaveChanges();
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                {
                    System.Data.Entity.Validation.DbEntityValidationException mEx = ex;
                }
                return RedirectToAction("Index");
            }            
            ViewBag.seccion = "Inspecciones";
            ViewBag.view = "Crear Inspeccion";
            ViewBag.buque = inspeccion.Buque1.nombre;
            //HERE BUQUES
            //ViewBag.buque = new SelectList(buques, "vesselID", "vesselName");  
            //ViewBag.representanteTer = new SelectList(db.Representante.Where(x => x.tipoRepresentante == "1"), "idRepresentante", "nombre");
            //ViewBag.representanteBuq = new SelectList(db.Representante.Where(x => x.tipoRepresentante == "2"), "idRepresentante", "nombre");
            ViewBag.segmentos = new List<Segmentos>(db.Segmentos.Where(x => x.activa == "A")).OrderBy(x => x.orden).ToList();
            ViewBag.turno = new SelectList(new string[] { "A", "B", "C" });
            List<int> codesId = inspeccion.Respuestas.Select(x => x.pregunta).ToList();
            ViewBag.preguntas = db.Preguntas.Where(x => codesId.Contains(x.idPregunta)).Where(x => x.activa == "A").ToList();
            return View(mInspeccion);
        }

        public ActionResult Adjunto(FormCollection formData)
        {
            HttpPostedFileBase file = Request.Files[0];
            string mPath = "";
            if (file.FileName != "")
            {
                mPath = Server.MapPath("~/") + "Views\\Inspeccion\\Files\\" + file.FileName;
                file.SaveAs(mPath);
            }
            var adjunto = new Adjunto();
            adjunto.fecha = DateTime.Now;
            adjunto.idInspeccion = Convert.ToInt32(Request["hiddenId"]);
            adjunto.usuario = User.Identity.Name;
            adjunto.comentario = Request["comment"];
            if (file.FileName != "") adjunto.path = "Files\\" + file.FileName; else adjunto.path = "";
            db.Adjunto.Add(adjunto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult PartialAdjuntos(int id)
        {        
            var inspeccion = db.Inspeccion.First(x => x.idInspeccion == id);
            ViewBag.buque = inspeccion.Buque1.nombre;
            ViewBag.fecha = inspeccion.fecha;
            ViewBag.visita = inspeccion.visita;
            ViewBag.repTer = inspeccion.Representante.nombre;
            ViewBag.repBuque = inspeccion.Representante1.nombre;
            var adjunto = db.Adjunto.Where(x => x.idInspeccion == id);
            return PartialView("_PartialAdjuntos", inspeccion);
        }

        [HttpGet]
        public ActionResult PartialNoConformidad(int id)
        {
            ViewBag.buque = new SelectList(db.Buque, "idBuque", "nombre");
            ViewBag.idInspeccion = new SelectList(db.Inspeccion, "idInspeccion", "visita");
            return PartialView("_NoConformidad");
        }

        [HttpPost]
        public ActionResult PartialNoConformidad([Bind(Include = "idNoConformidad,idInspeccion,fecha,nroContenedor,buque,noSello,danoContenedor,etiquetaMercancia,danoguias,otro,detalles")] NoConformidad mNoConformidad)
        {
            var inspeccion = db.Inspeccion.First(x => x.idInspeccion == mNoConformidad.idInspeccion);
            mNoConformidad.buque = inspeccion.buque;
            mNoConformidad.fecha = DateTime.Now;
            if (ModelState.IsValid)
            {
                db.NoConformidad.Add(mNoConformidad);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.buque = new SelectList(db.Buque, "idBuque", "nombre", mNoConformidad.buque);
            ViewBag.idInspeccion = new SelectList(db.Inspeccion, "idInspeccion", "visita", mNoConformidad.idInspeccion);
            return PartialView("_NoConformidad", mNoConformidad);
        }

        public FileResult GetFile(int id)
        {
            string path = Server.MapPath("~/") + "Views\\Inspeccion\\" + db.Adjunto.First(x => x.idAdjunto == id).path;
            return File(path, System.Net.Mime.MediaTypeNames.Application.Octet, Path.GetFileName(path));
        }

        // GET: /Default2/Delete/5
        [Authorize(Roles = " ehs_insp")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inspeccion inspeccion = db.Inspeccion.Find(id);
            if (inspeccion == null)
            {
                return HttpNotFound();
            }
            return View(inspeccion);
        }

        // POST: /Default2/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "ehs_insp")]
        public ActionResult DeleteConfirmed(int id)
        {
            Inspeccion inspeccion = db.Inspeccion.Find(id);
            db.Inspeccion.Remove(inspeccion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
