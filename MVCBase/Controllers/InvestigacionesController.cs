﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.IO;
using MVCBase;

namespace MVCBase.Controllers
{
    public class InvestigacionesController : Controller
    {
        private EHSEntities db = new EHSEntities();

        // GET: /Investigaciones/
        public ActionResult Index()
        {
            var investigacion = db.Investigacion.Include(i => i.CausaLesion1).Include(i => i.Entidad).Include(i => i.TipoLesion1);
            ViewBag.seccion = "Investigaciones";
            ViewBag.view = "Listado de Investigaciones";
            return View(investigacion.ToList());
        }

        // GET: /Investigaciones/Details/5
        public ActionResult Details(int? id)
        {
            rptInvestigacion mRep = new rptInvestigacion();
            mRep.SetParameterValue("idInvestigacion", id);
            mRep.SetDatabaseLogon("ehsusr", "ehs123", "192.168.95.21", "EHS", true);
            //mRep.SetDatabaseLogon("sarahtest", "opc123", "192.168.95.11", "EHS", true);
            return new FileStreamResult(mRep.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat), "application/pdf");
        }

        // GET: /Investigaciones/Create
        public ActionResult Create()
        {
            ViewBag.causaLesion = new SelectList(db.CausaLesion, "idCausa", "tipoCausa");
            ViewBag.departamento = new SelectList(db.Departamento, "idDepartemento", "nombre");
            ViewBag.representantes = new SelectList(db.Representante.Where(x => x.tipoRepresentante == "1"), "idRepresentante", "nombre");
            ViewBag.empresa = new SelectList(db.Entidad, "idEntidad", "entidad1");
            ViewBag.tipoLesion = new SelectList(db.TipoLesion, "idLesion", "tipoLesion1");
            ViewBag.status = new SelectList(new SelectListItem[]  { 
                                                    new SelectListItem { Text = "Permanente", Value = "1" }, 
                                                    new SelectListItem { Text = "Contratista", Value = "2" }, 
                                                    new SelectListItem { Text = "Visitante", Value = "3" }, 
                                                    new SelectListItem { Text = "Proveedor", Value = "4" } 
                                              }, "Value", "Text");
            ViewBag.probabilidad = new SelectList(new SelectListItem[]  { 
                                                    new SelectListItem { Text = "Muy Probable", Value = "1" }, 
                                                    new SelectListItem { Text = "Probable", Value = "2" }, 
                                                    new SelectListItem { Text = "Ocasional", Value = "3" }, 
                                                    new SelectListItem { Text = "Remoto", Value = "4" },
                                                    new SelectListItem { Text = "Improbable", Value = "5" } 
                                              }, "Value", "Text");
            ViewBag.gravedad = new SelectList(new SelectListItem[]  { 
                                                    new SelectListItem { Text = "Leve", Value = "1" }, 
                                                    new SelectListItem { Text = "Moderado", Value = "2" }, 
                                                    new SelectListItem { Text = "Severo", Value = "3" }, 
                                                    new SelectListItem { Text = "Fatal", Value = "4" } 
                                              }, "Value", "Text");
            ViewBag.seccion = "Investigaciones";
            ViewBag.view = "Crear Investigacion";
            return View();
        }

        // POST: /Investigaciones/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idInvestigacion,titulo,empresa,fechaHora,nombre,departamento,personaInformo,identidad,edad,codEmpleado,puesto,antiguedadEmpresa,AntiguedadPuesto,trabajoRealizaba,eraPuestoTrabajo,trabajoEncomendado,incapacidadMedica,diasIncapacidad,lugarLesiono,tipoLesion,causaLesion,status,probabilidad,infoMedica,gravedad,objetocauso,descripcion,adecuado,adecuadoComentario,usoCorrecto,usoCorrectoComentario,sugerido,sugeridoComentario,analisisCausa,DeclaracionInvestigacion,AccionCorrectivaInvestigacion,EvaluacionRevision")] Investigacion investigacion)
        {
            if (ModelState.IsValid)
            {
                investigacion.departamento = db.Departamento.FirstOrDefault(x => x.nombre == "EHS").idDepartemento;
                db.Investigacion.Add(investigacion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.causaLesion = new SelectList(db.CausaLesion, "idCausa", "tipoCausa", investigacion.causaLesion);
            ViewBag.departamento = new SelectList(db.Departamento, "idDepartemento", "nombre");
            ViewBag.representantes = new SelectList(db.Representante.Where(x => x.tipoRepresentante == "1"), "idRepresentante", "nombre");
            ViewBag.empresa = new SelectList(db.Entidad, "idEntidad", "entidad1", investigacion.empresa);
            ViewBag.tipoLesion = new SelectList(db.TipoLesion, "idLesion", "tipoLesion1", investigacion.tipoLesion);
            ViewBag.status = new SelectList(new SelectListItem[]  { 
                                                    new SelectListItem { Text = "Permanente", Value = "1" }, 
                                                    new SelectListItem { Text = "Contratista", Value = "2" }, 
                                                    new SelectListItem { Text = "Visitante", Value = "3" }, 
                                                    new SelectListItem { Text = "Proveedor", Value = "4" } 
                                              }, "Value", "Text");
            ViewBag.probabilidad = new SelectList(new SelectListItem[]  { 
                                                    new SelectListItem { Text = "Muy Probable", Value = "1" }, 
                                                    new SelectListItem { Text = "Probable", Value = "2" }, 
                                                    new SelectListItem { Text = "Ocasional", Value = "3" }, 
                                                    new SelectListItem { Text = "Remoto", Value = "4" },
                                                    new SelectListItem { Text = "Improbable", Value = "5" } 
                                              }, "Value", "Text");
            ViewBag.gravedad = new SelectList(new SelectListItem[]  { 
                                                    new SelectListItem { Text = "Leve", Value = "1" }, 
                                                    new SelectListItem { Text = "Moderado", Value = "2" }, 
                                                    new SelectListItem { Text = "Severo", Value = "3" }, 
                                                    new SelectListItem { Text = "Fatal", Value = "4" } 
                                              }, "Value", "Text");
            ViewBag.seccion = "Investigaciones";
            ViewBag.view = "Crear Investigacion";
            return View(investigacion);
        }

        // GET: /Investigaciones/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Investigacion investigacion = db.Investigacion.Find(id);
            if (investigacion == null)
            {
                return HttpNotFound();
            }
            ViewBag.causaLesion = new SelectList(db.CausaLesion, "idCausa", "tipoCausa", investigacion.causaLesion);
            ViewBag.departamento = new SelectList(db.Departamento, "idDepartemento", "nombre");
            ViewBag.representantes = new SelectList(db.Representante.Where(x => x.tipoRepresentante == "1"), "idRepresentante", "nombre");
            ViewBag.empresa = new SelectList(db.Entidad, "idEntidad", "entidad1", investigacion.empresa);
            ViewBag.tipoLesion = new SelectList(db.TipoLesion, "idLesion", "tipoLesion1", investigacion.tipoLesion);
            ViewBag.status = new SelectList(new SelectListItem[]  { 
                                                    new SelectListItem { Text = "Permanente", Value = "1" }, 
                                                    new SelectListItem { Text = "Contratista", Value = "2" }, 
                                                    new SelectListItem { Text = "Visitante", Value = "3" }, 
                                                    new SelectListItem { Text = "Proveedor", Value = "4" } 
                                              }, "Value", "Text");
            ViewBag.probabilidad = new SelectList(new SelectListItem[]  { 
                                                    new SelectListItem { Text = "Muy Probable", Value = "1" }, 
                                                    new SelectListItem { Text = "Probable", Value = "2" }, 
                                                    new SelectListItem { Text = "Ocasional", Value = "3" }, 
                                                    new SelectListItem { Text = "Remoto", Value = "4" },
                                                    new SelectListItem { Text = "Improbable", Value = "5" } 
                                              }, "Value", "Text");
            ViewBag.gravedad = new SelectList(new SelectListItem[]  { 
                                                    new SelectListItem { Text = "Leve", Value = "1" }, 
                                                    new SelectListItem { Text = "Moderado", Value = "2" }, 
                                                    new SelectListItem { Text = "Severo", Value = "3" }, 
                                                    new SelectListItem { Text = "Fatal", Value = "4" } 
                                              }, "Value", "Text");
            ViewBag.seccion = "Investigaciones";
            ViewBag.view = "Editar Investigacion";
            return View(investigacion);
        }

        // POST: /Investigaciones/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idInvestigacion,fechahora,titulo,empresa,departamento,personaInformo,identidad,edad,codEmpleado,puesto,antiguedadEmpresa,AntiguedadPuesto,trabajoRealizaba,eraPuestoTrabajo,trabajoEncomendado,incapacidadMedica,diasIncapacidad,lugarLesiono,tipoLesion,causaLesion,status,probabilidad,infoMedica,gravedad,objetocauso,descripcion,adecuado,adecuadoComentario,usoCorrecto,usoCorrectoComentario,sugerido,sugeridoComentario,analisisCausa,DeclaracionInvestigacion,AccionCorrectivaInvestigacion,EvaluacionRevision")] Investigacion investigacion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(investigacion).State = EntityState.Modified;
                foreach (var item in investigacion.DeclaracionInvestigacion) db.Entry(item).State = (item.idDeclaracion == 0) ? EntityState.Added : EntityState.Modified;
                foreach (var item in investigacion.AccionCorrectivaInvestigacion) db.Entry(item).State = (item.idAccionesCorrectiva == 0) ? EntityState.Added : EntityState.Modified;
                foreach (var item in investigacion.EvaluacionRevision) db.Entry(item).State = (item.idEvaluacion == 0) ? EntityState.Added : EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.causaLesion = new SelectList(db.CausaLesion, "idCausa", "tipoCausa", investigacion.causaLesion);
            ViewBag.departamento = new SelectList(db.Departamento, "idDepartemento", "nombre");
            ViewBag.representantes = new SelectList(db.Representante.Where(x => x.tipoRepresentante == "1"), "idRepresentante", "nombre");
            ViewBag.empresa = new SelectList(db.Entidad, "idEntidad", "entidad1", investigacion.empresa);
            ViewBag.tipoLesion = new SelectList(db.TipoLesion, "idLesion", "tipoLesion1", investigacion.tipoLesion);
            ViewBag.status = new SelectList(new SelectListItem[]  { 
                                                    new SelectListItem { Text = "Permanente", Value = "1" }, 
                                                    new SelectListItem { Text = "Contratista", Value = "2" }, 
                                                    new SelectListItem { Text = "Visitante", Value = "3" }, 
                                                    new SelectListItem { Text = "Proveedor", Value = "4" } 
                                              }, "Value", "Text");
            ViewBag.probabilidad = new SelectList(new SelectListItem[]  { 
                                                    new SelectListItem { Text = "Muy Probable", Value = "1" }, 
                                                    new SelectListItem { Text = "Probable", Value = "2" }, 
                                                    new SelectListItem { Text = "Ocasional", Value = "3" }, 
                                                    new SelectListItem { Text = "Remoto", Value = "4" },
                                                    new SelectListItem { Text = "Improbable", Value = "5" } 
                                              }, "Value", "Text");
            ViewBag.gravedad = new SelectList(new SelectListItem[]  { 
                                                    new SelectListItem { Text = "Leve", Value = "1" }, 
                                                    new SelectListItem { Text = "Moderado", Value = "2" }, 
                                                    new SelectListItem { Text = "Severo", Value = "3" }, 
                                                    new SelectListItem { Text = "Fatal", Value = "4" } 
                                              }, "Value", "Text");
            ViewBag.seccion = "Investigaciones";
            ViewBag.view = "Editar Investigacion";
            return View(investigacion);
        }

        public void deleteDeclaracionInvestigacion(int id)
        {
            db.DeclaracionInvestigacion.Remove(db.DeclaracionInvestigacion.Find(id));
            db.SaveChanges();
        }

        public void deleteAccionInvestigacion(int id)
        {
            db.AccionCorrectivaInvestigacion.Remove(db.AccionCorrectivaInvestigacion.Find(id));
            db.SaveChanges();
        }

        public void deleteEvaluacion(int id)
        {
            db.EvaluacionRevision.Remove(db.EvaluacionRevision.Find(id));
            db.SaveChanges();
        }       

        public JsonResult getMeta4InfoById(string cedula)
        {
            return Json(new Meta4Srv.META4Client().getEmployeeInfoById(cedula), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getMeta4InfoByCode(string code)
        {
            return Json(new Meta4Srv.META4Client().getEmployeeInfoByCode(code), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getDriverInfo(string cedula)
        {
            return Json(new BuquesSrv.N4Client().GetDrivers(cedula), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getMedicalPersonInfoId(string cedula)
        {
            return Json(new MedicalSrv.MedicalClient().getHistorialMedicoIdentidad(cedula), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getMedicalPersonInfoCode(string code)
        {
            return Json(new MedicalSrv.MedicalClient().getHistorialMedicoIdEmpleado(code), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Adjunto(FormCollection formData)
        {
            HttpPostedFileBase file = Request.Files[0];
            string mPath = "";
            if (file.FileName != "")
            {
                mPath = Server.MapPath("~/") + "Views\\Investigaciones\\Files\\" + file.FileName;
                file.SaveAs(mPath);
            }
            var adjunto = new AdjuntoInvestigacion();
            adjunto.fecha = DateTime.Now;
            adjunto.idInvestigacion = Convert.ToInt32(Request["hiddenId"]);
            adjunto.usuario = User.Identity.Name;
            adjunto.comentario = Request["comment"];
            if (file.FileName != "") adjunto.path = "Files\\" + file.FileName; else adjunto.path = "";
            db.AdjuntoInvestigacion.Add(adjunto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult PartialAdjuntos(int id)
        {
            var investigacion = db.Investigacion.First(x => x.idInvestigacion == id);
            ViewBag.titulo = investigacion.titulo;
            ViewBag.departamento = investigacion.Departamento1.nombre;
            ViewBag.entidad = investigacion.Entidad.entidad1;
            ViewBag.personaInformo = investigacion.personaInformo;
            ViewBag.fecha = investigacion.fechaHora;
            var adjunto = db.AdjuntoIncidenteDano.Where(x => x.idIncidenteDano == id);
            return PartialView("_PartialAdjuntos", investigacion);
        }

        public FileResult GetFile(int id)
        {
            string path = Server.MapPath("~/") + "Views\\Investigaciones\\" + db.AdjuntoInvestigacion.First(x => x.idAdjunto == id).path;
            return File(path, System.Net.Mime.MediaTypeNames.Application.Octet, Path.GetFileName(path));
        }

        // GET: /Investigaciones/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Investigacion investigacion = db.Investigacion.Find(id);
            if (investigacion == null)
            {
                return HttpNotFound();
            }
            return View(investigacion);
        }

        public Investigacion returnModel(int id)
        {
            return db.Investigacion.First(x => x.idInvestigacion == id);
        }

        // POST: /Investigaciones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Investigacion investigacion = db.Investigacion.Find(id);
            db.Investigacion.Remove(investigacion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
