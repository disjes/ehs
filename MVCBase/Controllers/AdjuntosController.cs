﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCBase;

namespace MVCBase.Controllers
{
    public class AdjuntosController : Controller
    {
        private EHSEntities db = new EHSEntities();

        // GET: /Default1/
        public ActionResult Index()
        {
            var adjunto = db.Adjunto.Include(a => a.Inspeccion);
            return View(adjunto.ToList());
        }

        // GET: /Default1/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Adjunto adjunto = db.Adjunto.Find(id);
            if (adjunto == null)
            {
                return HttpNotFound();
            }
            return View(adjunto);
        }

        // GET: /Default1/Create
        public ActionResult Create()
        {
            ViewBag.idInspeccion = new SelectList(db.Inspeccion, "idInspeccion", "visita");
            return View();
        }

        // POST: /Default1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="idAdjunto,idInspeccion,fecha,usuario,comentario,path")] Adjunto adjunto)
        {
            if (ModelState.IsValid)
            {
                db.Adjunto.Add(adjunto);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idInspeccion = new SelectList(db.Inspeccion, "idInspeccion", "visita", adjunto.idInspeccion);
            return View(adjunto);
        }

        // GET: /Default1/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Adjunto adjunto = db.Adjunto.Find(id);
            if (adjunto == null)
            {
                return HttpNotFound();
            }
            ViewBag.idInspeccion = new SelectList(db.Inspeccion, "idInspeccion", "visita", adjunto.idInspeccion);
            return View(adjunto);
        }

        // POST: /Default1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="idAdjunto,idInspeccion,fecha,usuario,comentario,path")] Adjunto adjunto)
        {
            if (ModelState.IsValid)
            {
                db.Entry(adjunto).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idInspeccion = new SelectList(db.Inspeccion, "idInspeccion", "visita", adjunto.idInspeccion);
            return View(adjunto);
        }

        // GET: /Default1/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Adjunto adjunto = db.Adjunto.Find(id);
            if (adjunto == null)
            {
                return HttpNotFound();
            }
            return View(adjunto);
        }

        // POST: /Default1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Adjunto adjunto = db.Adjunto.Find(id);
            db.Adjunto.Remove(adjunto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
