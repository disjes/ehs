﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.IO;
using MVCBase;
using MVCBase.Meta4Srv;
using MVCBase.MedicalSrv;
using MVCBase.BuquesSrv;
using Newtonsoft.Json;
using System.Runtime.Serialization.Json;
using CrystalDecisions.CrystalReports.Engine;
namespace MVCBase.Controllers
{
    public class DanosController : Controller
    {
        private EHSEntities db = new EHSEntities();

        // GET: /Danos/
        public ActionResult Index()
        {            
            var incidentesdanos = db.IncidentesDanos.Include(i => i.Lugar).Include(i => i.Representante).Include(i => i.Representante1);
            ViewBag.seccion = "Daños";
            ViewBag.view = "Listado de Daños";
            return View(incidentesdanos.ToList());
        }

        // GET: /Danos/Details/5
        public ActionResult Details(int? id)
        {
            rptDaños mRep = new rptDaños();            
            mRep.SetParameterValue("idDano", id);
            mRep.SetDatabaseLogon("ehsusr", "ehs123", "192.168.95.21", "EHS", true);
            foreach (ReportDocument subReport in mRep.Subreports) subReport.SetDatabaseLogon("ehsusr", "ehs123", "192.168.95.21", "EHS", true);
            //mRep.SetDatabaseLogon("sarahtest", "opc123", "192.168.95.11", "EHS", true);
            return new FileStreamResult(mRep.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat), "application/pdf");
        }

        // GET: /Danos/Create
        public ActionResult Create()
        {
            ViewBag.ubicacion = new SelectList(db.Lugar, "idLugar", "nombre");
            ViewBag.buques = new SelectList(new BuquesSrv.N4Client().GetVessels(), "vesselID", "vesselName");
            ViewBag.entidades = new SelectList(db.Entidad, "idEntidad", "entidad1");
            ViewBag.investigadoPor = new SelectList(db.Representante.Where(x => x.tipoRepresentante == "1"), "idRepresentante", "nombre");
            ViewBag.representanteAccion = new SelectList(db.Representante.Where(x => x.tipoRepresentante == "1"), "idRepresentante", "nombre");
            ViewBag.responsableArea = new SelectList(db.Representante.Where(x => x.tipoRepresentante == "3"), "idRepresentante", "nombre");
            ViewBag.seccion = "Daños";
            ViewBag.view = "Crear Daños";
            return View();
        }

        // POST: /Danos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idDano,titulo,ubicacion,responsableArea,personalInformo,fechaHora,descripcionIncidente,investigadoPor,fechaInvestigo,analisisCausa,InfoDanos,AccionCorrectivaEHS,DeclaracionDanoIncidente,Entidad,InfoDanos,PersonalInvolucradoDano")] IncidentesDanos incidentesdanos)
        {
            if (ModelState.IsValid)
            {
                incidentesdanos.fechaHora = null;
                performClearAndUpdates(incidentesdanos);
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-GB");
                incidentesdanos.fechaHora = DateTime.Now;
                db.IncidentesDanos.Add(incidentesdanos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }         
            ViewBag.ubicacion = new SelectList(db.Lugar, "idLugar", "nombre");           
            ViewBag.buques = new SelectList(new BuquesSrv.N4Client().GetVessels(), "vesselID", "vesselName");
            ViewBag.entidades = new SelectList(db.Entidad, "idEntidad", "entidad1");
            ViewBag.investigadoPor = new SelectList(db.Representante.Where(x => x.tipoRepresentante == "1"), "idRepresentante", "nombre");
            ViewBag.representanteAccion = new SelectList(db.Representante.Where(x => x.tipoRepresentante == "1"), "idRepresentante", "nombre");
            ViewBag.responsableArea = new SelectList(db.Representante.Where(x => x.tipoRepresentante == "3"), "idRepresentante", "nombre");
            ViewBag.seccion = "Daños";
            ViewBag.view = "Crear Daños";
            return View(incidentesdanos);
        }

        private void performClearAndUpdates(IncidentesDanos incidentesdanos)
        {
            clearInfos(incidentesdanos);
            List<Entidad> entidades = new List<Entidad>();
            foreach (var entidad in incidentesdanos.Entidad) entidades.Add(db.Entidad.First(x => x.idEntidad == entidad.idEntidad));
            incidentesdanos.Entidad = entidades;
        }

        private void clearInfos(IncidentesDanos incidentesdanos)
        {
            string[] categories = new[] { "contenedor", "buque", "equipo", "instalacion"};
            string[] properties = new[] { "informacion", "informacion", "equipo", "instalacion"};
            for (int i = 0; i < categories.Length; i++)
            {
                var items = incidentesdanos.InfoDanos.Where(x => x.incidenteTipo == categories[i]).ToList();
                for (int j = 0; j < items.Count; j++)
                {
                    if (items[j].GetType().GetProperty(properties[i]).GetValue(items[j], null) == null)
                        incidentesdanos.InfoDanos.Remove(items[j]);
                }
            }
        }

        // GET: /Danos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IncidentesDanos incidentesdanos = db.IncidentesDanos.Find(id);
            if (incidentesdanos == null)
            {
                return HttpNotFound();
            }
            ViewBag.ubicacion = new SelectList(db.Lugar, "idLugar", "nombre");
            ViewBag.buques = new SelectList(db.Buque, "idBuque", "nombre");
            ViewBag.entidades = new SelectList(db.Entidad, "idEntidad", "entidad1");
            ViewBag.investigadoPor = new SelectList(db.Representante.Where(x => x.tipoRepresentante == "1"), "idRepresentante", "nombre");
            ViewBag.representanteAccion = new SelectList(db.Representante.Where(x => x.tipoRepresentante == "1"), "idRepresentante", "nombre");
            ViewBag.responsableArea = new SelectList(db.Representante.Where(x => x.tipoRepresentante == "3"), "idRepresentante", "nombre");
            ViewBag.seccion = "Daños";
            ViewBag.view = "Editar Daños";
            return View(incidentesdanos);
        }

        // POST: /Danos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId =317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idDano,titulo,ubicacion,responsableArea,personalInformo,fechaHora,descripcionIncidente,investigadoPor,fechaInvestigo,analisisCausa,InfoDanos,AccionCorrectivaEHS,DeclaracionDanoIncidente,Entidad,InfoDanos,PersonalInvolucradoDano")] IncidentesDanos incidentesdanos)
        {
            if (ModelState.IsValid)
            {
                performClearAndUpdates(incidentesdanos);
                db.Entry(incidentesdanos).State = EntityState.Modified;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-GB");
                foreach (var item in incidentesdanos.AccionCorrectivaEHS) db.Entry(item).State = (item.idAccionCorrectiva == 0) ? EntityState.Added : EntityState.Modified;
                foreach (var item in incidentesdanos.DeclaracionDanoIncidente) db.Entry(item).State = (item.idDeclaracion == 0) ? EntityState.Added : EntityState.Modified;
                foreach (var item in incidentesdanos.Entidad) db.Entry(item).State = (item.idEntidad == 0) ? EntityState.Added : EntityState.Modified;
                foreach (var item in incidentesdanos.InfoDanos) db.Entry(item).State = (item.idInfoDano == 0) ? EntityState.Added : EntityState.Modified;
                foreach (var item in incidentesdanos.PersonalInvolucradoDano) db.Entry(item).State = (item.idPersonalinvol == 0) ? EntityState.Added : EntityState.Modified;                
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ubicacion = new SelectList(db.Lugar, "idLugar", "nombre");
            ViewBag.buques = new SelectList(new BuquesSrv.N4Client().GetVessels(), "vesselID", "vesselName");
            ViewBag.entidades = new SelectList(db.Entidad, "idEntidad", "entidad1");
            ViewBag.investigadoPor = new SelectList(db.Representante.Where(x => x.tipoRepresentante == "1"), "idRepresentante", "nombre");
            ViewBag.representanteAccion = new SelectList(db.Representante.Where(x => x.tipoRepresentante == "1"), "idRepresentante", "nombre");
            ViewBag.responsableArea = new SelectList(db.Representante.Where(x => x.tipoRepresentante == "3"), "idRepresentante", "nombre");
            ViewBag.seccion = "Daños";
            ViewBag.view = "Editar Daños";
            return View(incidentesdanos);
        }

        public void deleteEntidades(int id, int dano)
        {
            db.IncidentesDanos.Find(dano).Entidad.Remove(db.Entidad.First(x => x.idEntidad == id));
            db.SaveChanges();
        }

        public void deleteInfoDano(int id, int dano)
        {
            db.InfoDanos.Remove(db.InfoDanos.Find(id));
            db.SaveChanges();
        }

        public void deletePersonal(int id, int dano)
        {
            db.PersonalInvolucradoDano.Remove(db.PersonalInvolucradoDano.Find(id));
            db.SaveChanges();
        }

        public void deleteDeclaracion(int id, int dano)
        {
            db.DeclaracionDanoIncidente.Remove(db.DeclaracionDanoIncidente.Find(id));
            db.SaveChanges();
        }

        public void deleteAccion(int id, int dano)
        {
            db.AccionCorrectivaEHS.Remove(db.AccionCorrectivaEHS.Find(id));
            db.SaveChanges();
        }

        public string getVesselInfo(string VesselId)
        {
            return new N4Client().getVessel(VesselId);
        }

        public string getVesselVisitId(string VesselId)
        {
            return new N4Client().getVesselVisitId(VesselId);
        }

        public string getUnitInfo(string nroContenedor)
        {
            return new N4Client().GetUnits(nroContenedor);
        }

        public JsonResult getMeta4InfoById(string cedula)
        {
            try
            {
                var info = new Meta4Srv.META4Client().getEmployeeInfoById(cedula);
                return Json(info, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                string message = ex.Message;
            }
            return null;
        }

        public JsonResult getDriverInfo(string cedula)
        {
            return Json(new BuquesSrv.N4Client().GetDrivers(cedula));
        }

        public JsonResult getMedicalPersonInfoId(string cedula)
        {
            return Json(new MedicalSrv.MedicalClient().getHistorialMedicoIdentidad(cedula));
        }

        public ActionResult Adjunto(FormCollection formData)
        {
            HttpPostedFileBase file = Request.Files[0];
            string mPath = "";
            if (file.FileName != "")
            {
                mPath = Server.MapPath("~/") + "Views\\Danos\\Files\\" + file.FileName;
                file.SaveAs(mPath);
            }
            var adjunto = new AdjuntoIncidenteDano();
            adjunto.fecha = DateTime.Now;
            adjunto.idIncidenteDano = Convert.ToInt32(Request["hiddenId"]);
            adjunto.usuario = User.Identity.Name;
            adjunto.comentario = Request["comment"];
            if (file.FileName != "") adjunto.path = "Files\\" + file.FileName; else adjunto.path = "";
            db.AdjuntoIncidenteDano.Add(adjunto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult PartialAdjuntos(int id)
        {
            var dano = db.IncidentesDanos.First(x => x.idDano == id);
            ViewBag.titulo = dano.titulo;
            ViewBag.ubicacion = dano.ubicacion;
            ViewBag.responsable = dano.Representante1.nombre;
            ViewBag.personal = dano.personalInformo;
            ViewBag.fecha = dano.fechaHora;
            var adjunto = db.AdjuntoIncidenteDano.Where(x => x.idIncidenteDano == id);
            return PartialView("_PartialAdjuntos", dano);
        }

        public FileResult GetFile(int id)
        {
            string path = Server.MapPath("~/") + "Views\\Danos\\" + db.AdjuntoIncidenteDano.First(x => x.idAdjunto == id).path;
            return File(path, System.Net.Mime.MediaTypeNames.Application.Octet, Path.GetFileName(path));
        }

        // GET: /Danos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IncidentesDanos incidentesdanos = db.IncidentesDanos.Find(id);
            if (incidentesdanos == null)
            {
                return HttpNotFound();
            }
            return View(incidentesdanos);
        }

        // POST: /Danos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            IncidentesDanos incidentesdanos = db.IncidentesDanos.Find(id);
            db.IncidentesDanos.Remove(incidentesdanos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
